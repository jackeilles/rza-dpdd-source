from typing import Optional
import sqlalchemy as sa 
import sqlalchemy.orm as so
from app import db
from sqlalchemy_json import MutableJson
from flask_login import UserMixin
from app import login
from werkzeug.security import check_password_hash, generate_password_hash
import datetime

class User(UserMixin, db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    forename: so.Mapped[str] = so.mapped_column(sa.String(32), index=True)
    surname: so.Mapped[str] = so.mapped_column(sa.String(32), index=True)
    email: so.Mapped[str] = so.mapped_column(sa.String(64), index=True, unique=True)
    password_hash: so.Mapped[Optional[str]] = so.mapped_column(sa.String(256))
    home_num: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    home_street: so.Mapped[str] = so.mapped_column(sa.String(64), index=True, nullable=True)
    home_town: so.Mapped[str] = so.mapped_column(sa.String(128), index=True, nullable=True)
    home_postcode: so.Mapped[str] = so.mapped_column(sa.String(8), index=True, nullable=True)
    opendyslexic: so.Mapped[bool] = so.mapped_column(sa.Boolean(), index=True, nullable=True)
    points: so.Mapped[int] = so.mapped_column(nullable=True)
    
    def __repr__(self):
        return f'<User {self.forename} {self.surname}>'
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

class HotelBookings(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    user_id: so.Mapped[int] = so.mapped_column(sa.ForeignKey(User.id), index=True)
    booking_info: so.Mapped[str] = so.mapped_column(MutableJson)
    has_zoo_booking: so.Mapped[bool] = so.mapped_column(sa.Boolean(), index=True)
    awaiting_payment: so.Mapped[bool] = so.mapped_column(sa.Boolean(), index=True)
    
    def __repr__(self):
        return f'<HotelBookings {self.booking_info}>'
    
class ZooBookings(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    user_id: so.Mapped[int] = so.mapped_column(sa.ForeignKey(User.id), index=True)
    booking_info: so.Mapped[str] = so.mapped_column(MutableJson)
    has_hotel_booking: so.Mapped[bool] = so.mapped_column(sa.Boolean(), index=True)
    awaiting_payment: so.Mapped[bool] = so.mapped_column(sa.Boolean(), index=True)
    
    def __repr__(self):
        return f'<HotelBookings {self.booking_info}>'

class PaymentInfo(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    user_id: so.Mapped[int] = so.mapped_column(sa.ForeignKey(User.id), index=True)
    card_info: so.Mapped[str] = so.mapped_column(MutableJson)
    
    def __repr__(self):
        return f'<CardInfo {self.card_info}>'
    
class Statistics(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    date: so.Mapped[str] = so.mapped_column(sa.String(16), index=True)
    zoo_bookings: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    hotel_bookings: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    user_signups: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    site_visits: so.Mapped[int] = so.mapped_column(index=True, nullable=True)
    
class Msg(db.Model):
    id: so.Mapped[int] = so.mapped_column(primary_key=True)
    user_id: so.Mapped[int] = so.mapped_column(sa.ForeignKey(User.id), index=True)
    enquiry: so.Mapped[str] = so.mapped_column(sa.String(4096))
    response: so.Mapped[str] = so.mapped_column(sa.String(4096), nullable=True)
    
    
@login.user_loader
def load_user(id):
    return db.session.get(User, int(id))