from app import db
import sqlalchemy as sa
import sqlalchemy.orm as so
from app import models
import datetime
# Extra utility scripts

class Statistics():
    def increment_stats(type):
        stats = db.session.scalar(sa.select(models.Statistics).where(models.Statistics.date == datetime.datetime.now().date()))
        if stats is None:
            # Create todays stats.
            today = models.Statistics(date=datetime.datetime.now().date(), zoo_bookings=0, hotel_bookings=0, user_signups=0, site_visits=0)
            db.session.add(today)
            db.session.commit()
        today = models.Statistics.query.filter_by(date=str(datetime.datetime.now().date())).first()
        match type:
            case "zoo_bookings":
                today.zoo_bookings += 1
            case "hotel_bookings":
                today.hotel_bookings += 1
            case "user_signups":
                today.user_signups += 1
            case "site_visits":
                today.site_visits += 1
                
        db.session.commit()
        
        