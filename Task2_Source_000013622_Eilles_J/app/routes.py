# Holds all application routes.

from app import app
from flask import render_template, flash, redirect, url_for, request
from app.forms import LoginForm, RegistrationForm, HotelBookingForm, PaymentForm, ZooBookingForm, ProfileForm, ContactForm
from flask_login import current_user, login_user, logout_user, login_required
import sqlalchemy as sa 
from app import db
from app.models import User, HotelBookings, PaymentInfo, ZooBookings, Msg
from app import models # just to get stats
from app.worker import Statistics
import datetime
import random
import json

@app.route('/')
def index():
    Statistics.increment_stats("site_visits") # Increment stats
    return render_template("index.html", title="Home")

@app.route('/login', methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index')) # If the user is already logged in, throw them to the index.
    
    form = LoginForm() # Grab the form process
    if form.validate_on_submit(): # Has the form been submitted?
        # Search for the user in the db
        user = db.session.scalar(sa.select(User).where(User.email == form.email.data))
        # If it can't be found or if the password is wrong, prompt user
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password.')
            return redirect(url_for('login'))
        # Otherwise log in
        login_user(user, remember=form.remember_me.data)
        flash(f'Logging in {form.email.data}.')
        return redirect(url_for('index')) # Redirect home
    return render_template("login.html", form=form, title="Login")

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index')) # Line 18 comment pls thx
    
    form = RegistrationForm()
    if form.validate_on_submit():
        try: 
            form.check_email(form.email.data)
        except:
            flash("E-Mail already in use, please use another.")
            return redirect(url_for('register'))
        
        if form.forename.data.lower() == "admin":
            flash("Sorry, that is a restricted name, please choose another.")
            return redirect(url_for('register'))
        
        # Create the user entity
        user = User(forename=form.forename.data, surname=form.surname.data, email=form.email.data, points=0)
        user.set_password(form.password.data) # Set the password, refer to models.py
        
        # Add and commit to db
        db.session.add(user)
        db.session.commit()
        Statistics.increment_stats("user_signups")
        
        flash(f'Registering {form.email.data}.')
        return redirect(url_for('login'))
    return render_template("register.html", form=form, title="Register")

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/book')
@login_required
def book():
    return render_template("booking.html", title="Book")

@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    form = ProfileForm()
    points = db.session.scalar(sa.select(User).where(User.points != None))
    if points.points is None:
        points.points = 0
    else:
        points = points.points
    if form.validate_on_submit():
        profile = User.query.filter_by(id=current_user.id).first()
        
        profile.home_num = form.home_num.data if form.home_num.data else None
        profile.home_street = form.home_street.data if form.home_street.data else None
        profile.home_town = form.home_town.data if form.home_town.data else None 
        profile.home_postcode = form.home_postcode.data if form.home_postcode else None
        profile.opendyslexic = form.opendyslexic.data if form.opendyslexic.data else None
        
        db.session.add(profile)
        db.session.commit()
        flash("Saved Successfully") # All above is adding stuff to the profile
    return render_template("profile.html", title="Profile", form=form, points=points)

@app.route('/book/hotel', methods=['GET', 'POST'])
@login_required
def hotel_booking():
    form = HotelBookingForm()
    if form.validate_on_submit():
        
        # Booking info must follow this type of format
        info = {
            "full_name": f'{form.forename.data} {form.surname.data}',
            "people": form.people.data,
            "arrival": str(form.start_date.data),
            "departure": str(form.end_date.data)
        }
        book = HotelBookings(user_id=current_user.id, booking_info=info, has_zoo_booking=form.zoo_tickets.data, awaiting_payment=True)
        try:
            # Add and commit to db
            db.session.add(book)
            db.session.commit()
            flash("New Hotel Booking added to DB.")
            return redirect(f'{url_for('pay_hotel')}?id={book.id}') # make sure booking id is added onto end so we know which is being edited
        except Exception as e: # If it fails then rollback
            print(e)
            db.session.rollback()
            return render_template("hotel_booking.html", title="Book Hotel", form=form)
    return render_template("hotel_booking.html", title="Book Hotel", form=form)

@app.route('/book/hotel/pay', methods=['GET', 'POST'])
@login_required
def pay_hotel():
    if request.args.get('id') == None:
        return redirect(url_for('hotel_booking'))
    elif request.args.get('id'): # check if the booking has already been paid for
        pay_check = db.session.scalar(sa.select(HotelBookings).where(HotelBookings.id == request.args.get('id')))
        if pay_check.awaiting_payment == 0:
            return "This booking has already been paid for. \n <a href='/'>Go Home.</a>" # if it has then display error and go home
    
    form = PaymentForm()
    card = db.session.scalar(sa.select(PaymentInfo).where(PaymentInfo.user_id == current_user.id)) # grab obj of db
    
    if form.validate_on_submit():
        book = HotelBookings.query.filter_by(id=request.args.get('id'), user_id=current_user.id).first() # full editable object for db
        user = User.query.filter_by(id = current_user.id).first()
        book.awaiting_payment = 0 # set awaiting payment to 0, theyve paid now ^.^
        if form.save_info.data: # create card info stuff
                card_info = {
                    "cardholder_name": form.cardholder_name.data,
                    "card_num": form.card_num.data,
                    "card_cvv": form.card_cvv.data,
                    "card_expiry_mm": form.card_expiry_mm.data,
                    "card_expiry_yy": form.card_expiry_yy.data
                }
                card = PaymentInfo(user_id=current_user.id, card_info=card_info) # card info object made
                db.session.add(card) # add to db
        
        # Per booking, give 10 points
        if user.points == None: # fix for accounts with NULL points
            user.points = 10    
        else:
            user.points += 10
        
        db.session.add(book)
        db.session.commit()
        Statistics.increment_stats("hotel_bookings")
        flash(f"Booking for ID:{request.args.get('id')} paid for.")
        return redirect(url_for('booking_success'))
    
    # Work out final price
    if pay_check.has_zoo_booking:
        days = datetime.datetime.strptime(pay_check.booking_info['departure'], '%Y-%m-%d') - datetime.datetime.strptime(pay_check.booking_info['arrival'], '%Y-%m-%d')
        price = (pay_check.booking_info["people"] * ((65 * days.days)) + 25)
    else:
        days = datetime.datetime.strptime(pay_check.booking_info['departure'], '%Y-%m-%d') - datetime.datetime.strptime(pay_check.booking_info['arrival'], '%Y-%m-%d')
        price = pay_check.booking_info["people"] * (65 * days.days)
    
    if card is not None:
        return render_template("hotel_pay.html", form=form, title="Pay", card=card, price=price, days=days.days, people=pay_check.booking_info['people'])
    
    return render_template("hotel_pay.html", form=form, title="Pay", card=None, price=price, days=days.days, people=pay_check.booking_info['people'])

@app.route('/book/zoo', methods=['GET', 'POST'])
@login_required
def zoo_booking():
    form = ZooBookingForm()
    if form.validate_on_submit():
        
        # Booking info must follow this type of format
        info = {
            "full_name": f'{form.forename.data} {form.surname.data}',
            "people": form.people.data,
            "arrival": str(form.arrival.data),
        }
        book = ZooBookings(user_id=current_user.id, booking_info=info, has_hotel_booking=False, awaiting_payment=True)
        try:
            db.session.add(book)
            db.session.commit()
            flash("New Hotel Booking added to DB.")
            return redirect(f'{url_for('pay_zoo')}?id={book.id}')
        except Exception as e:
            print(e)
            db.session.rollback()
            return render_template("zoo_booking.html", title="Book Zoo", form=form)
    return render_template("zoo_booking.html", title="Book Zoo", form=form)

@app.route('/book/zoo/pay', methods=['GET', 'POST'])
@login_required
def pay_zoo():
    if request.args.get('id') == None:
        return redirect(url_for('zoo_booking'))
    elif request.args.get('id'):
        pay_check = db.session.scalar(sa.select(ZooBookings).where(ZooBookings.id == request.args.get('id')))
        if pay_check.awaiting_payment == 0:
            return "This booking has already been paid for. \n <a href='/'>Go Home.</a>"
    
    form = PaymentForm()
    card = db.session.scalar(sa.select(PaymentInfo).where(PaymentInfo.user_id == current_user.id))
    
    if form.validate_on_submit():
        book = ZooBookings.query.filter_by(id=request.args.get('id'), user_id=current_user.id).first()
        user = User.query.filter_by(id = current_user.id).first()
        print(book)
        book.awaiting_payment = 0
        if form.save_info.data:
                card_info = {
                    "cardholder_name": form.cardholder_name.data,
                    "card_num": form.card_num.data,
                    "card_cvv": form.card_cvv.data,
                    "card_expiry_mm": form.card_expiry_mm.data,
                    "card_expiry_yy": form.card_expiry_yy.data
                }
                card = PaymentInfo(user_id=current_user.id, card_info=card_info)
                db.session.add(card)

        if user.points == None: # fix for accounts with NULL points
            user.points = 10    
        else:
            user.points += 10
                
        db.session.add(book)
        db.session.commit()
        Statistics.increment_stats("zoo_bookings")
        flash(f"Booking for ID:{request.args.get('id')} paid for.")
        return redirect(url_for('booking_success'))
    
    # Let's work out the final cost too.
    price = int(pay_check.booking_info['people']) * 35
    
    if card is not None:
        return render_template("zoo_pay.html", form=form, title="Pay", card=card, people=pay_check.booking_info['people'], price=price)
    
    return render_template("zoo_pay.html", form=form, title="Pay", card=None, people=pay_check.booking_info['people'], price=price)

@app.route('/booking_success')
def booking_success():
    return render_template("booking_success.html", title="Success")

@app.route('/plan')
def plan():
    return render_template("plan.html", title="Plan your Trip.")

@app.route('/admin')
def admin():
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    
    # grab stats
    stats = db.session.scalar(sa.select(models.Statistics).where(models.Statistics.date == datetime.datetime.now().date()))
    
    return render_template('admin.html', stats=stats, title="Admin Dashboard")

@app.route('/admin/messages')
def admin_messages():
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    
    messages = Msg.query.all()
    return render_template('admin_messages.html', title="Admin Messages", messages=messages, message_count=len(messages))

@app.route('/admin/messages/<msgid>', methods=['GET', 'POST'])
def message_id(msgid):
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    
    msgid = int(msgid)
    
    message = Msg.query.filter_by(id = msgid).first()
    
    if message == None:
        return redirect(url_for('admin_messages'))
    
    form = ContactForm() # Using this form for replies too
    
    if form.validate_on_submit():
        message.response = form.text.data
        db.session.commit()
        return redirect('/admin/messages')
    
    return render_template('admin_message.html', title=f"id: {msgid}", message=message, form=form)

@app.route('/admin/messages/<msgid>/delete')
def del_message(msgid):
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    
    Msg.query.filter_by(id = msgid).delete()
    db.session.commit()
    
    return redirect(url_for('admin_messages'))

@app.route('/admin/check_booking/')
def check_booking():
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    
    return render_template('admin_check_booking.html', title="Check Bookings")

@app.route('/admin/check_hotel/<bookid>')
def check_hotel(bookid):
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    hotel = HotelBookings.query.filter_by(id = bookid).first()
    if hotel is None:
        return "Invalid Booking Reference! Go back now."
    
    # Grab zoo and paid
    if hotel.has_zoo_booking:
        zoo="Yes" 
    else:
        zoo="No" 
    if hotel.awaiting_payment:
        paid="No" 
    else:
        paid="Yes" 
    
    return render_template('check_hotel.html', name=hotel.booking_info['full_name'], bookid=bookid, ppl=hotel.booking_info['people'], arrival=hotel.booking_info['arrival'], departure=hotel.booking_info['departure'], zoo=zoo, paid=paid)

@app.route('/admin/check_zoo/<bookid>')
def check_zoo(bookid):
    if current_user.forename != "admin":
        flash("nuh uh")
        return redirect('/')
    zoo = ZooBookings.query.filter_by(id = bookid).first()
    if zoo is None:
        return "Invalid Booking Reference! Go back now."
    
    # Grab paid
    if zoo.awaiting_payment:
        paid="No" 
    else:
        paid="Yes" 
    
    return render_template('check_zoo.html', name=zoo.booking_info['full_name'], bookid=bookid, ppl=zoo.booking_info['people'], arrival=zoo.booking_info['arrival'], paid=paid)

 
@app.route('/contact', methods=['GET', 'POST'])
@login_required
def contact():
    form = ContactForm()
    
    if form.validate_on_submit():
        enquiry = Msg(user_id=current_user.id, enquiry=form.text.data)
        db.session.add(enquiry)
        db.session.commit()
        return redirect('/')
    
    return render_template('contact.html', form=form, title="Contact")

@app.route('/about')
def about():
    return render_template('about.html', title="About")

@app.route('/accessibility')
def accessibility():
    return render_template('accessibility.html', title="Accessibility")

@app.route('/pricing')
def pricing():
    return render_template('pricing.html', title="Pricing")

@app.route('/terms')
def terms():
    return render_template('terms.html', title="Terms")

@app.route('/privacy')
def privacy():
    return render_template('privacy.html', title='Privacy')

@app.route('/cookies')
def cookies():
    return render_template('cookies.html', title='Cookies')

@app.route('/placeholder/newsletter')
def newsletter():
    return "This would take you to a screen to sign up for their newsletter. As I don't have anything to send e-mails with, this will not be added. Please press the back button now."

######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################

## AICI Stuff
@app.route('/aici')
def aici():
    return render_template("aici.html")

@app.route('/api')
def api():
    return {'error': 'Please specify a version'}

# AI-CI API V1
@app.route('/api/v1')
def v1():
    return {'status': 'ok'}

@app.route('/api/v1/aici')
def aici_api():
    return {'error': 'Please specify a command'}

@app.route('/api/v1/aici/rawr')
def aicirawr():
    # As I quite obviously don't have access to real equipment for this
    # The AI-CI demonstration will be performed using random numbers.
    aici_data = {
        'aici': {
            'datacap': {
                'timestamp': datetime.datetime.now().timestamp(),
                'aici_zone_1': {
                    'max_assumed': random.randint(77, 80),
                    'low_assumed': random.randint(69, 76),
                },
                'aici_zone_2': {
                    'max_assumed': random.randint(42, 80),
                    'low_assumed': random.randint(2, 36),
                },
                'aici_zone_3': {
                    'max_assumed': random.randint(74, 102),
                    'low_assumed': random.randint(65, 73),
                },
            }
        }
    }
    return aici_data

@app.route('/api/v1/sound')
def sound():
    return {'error': 'Please specify a command'}

@app.route('/api/v1/sound/rawr')
def soundrawr():
    # As I quite obviously don't have access to real equipment for this
    # The sound demonstration will be performed using random numbers.
    sound_data = {
        'sound': {
            'datacap': {
                'timestamp': datetime.datetime.now().timestamp(),
                'sound_zone_1': {
                    'max_assumed': str(random.randint(77, 80)) + "db",
                    'low_assumed': str(random.randint(69, 76)) + "db",
                },
                'sound_zone_2': {
                    'max_assumed': str(random.randint(42, 80)) + "db",
                    'low_assumed': str(random.randint(2, 36)) + "db",
                },
                'sound_zone_3': {
                    'max_assumed': str(random.randint(74, 102)) + "db",
                    'low_assumed': str(random.randint(65, 73)) + "db",
                },
            }
        }
    }
    return sound_data