# Initialises all forms for the RZA Site

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, DateField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Length
import sqlalchemy as sa
from app import db
from app.models import User

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me?')
    submit = SubmitField('Login')
    
class RegistrationForm(FlaskForm):
    """ Information to create the User Registration Form on RZA.

    Args:
        FlaskForm (class): Created by flask_wtf

    Raises:
        ValidationError: Raised if a duplicate E-Mail address is discovered
    """
    forename = StringField('Forename', validators=[DataRequired()])
    surname = StringField('Surname', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')
    
    def check_email(self, email):
        """ Performs a db lookup on the email address provided to check if it matches an already provided one.

        Args:
            email (class): The provided E-Mail address from the form.
        """
        user = db.session.scalar(sa.select(User).where(
            User.email == self.email.data 
        ))
        if user is not None: # Has something actually been found???
            raise ValidationError()
        
class HotelBookingForm(FlaskForm):
    forename = StringField('Forename', validators=[DataRequired()])
    surname = StringField('Surname', validators=[DataRequired()])
    email = StringField('E-Mail for Confirmation', validators=[DataRequired(), Email()])
    people = IntegerField('Amount of people', validators=[DataRequired()])
    start_date = DateField('Start of Stay', validators=[DataRequired()])
    end_date = DateField('End of Stay', validators=[DataRequired()])
    zoo_tickets = BooleanField('Add zoo tickets onto your stay? £25 extra p.p')
    submit = SubmitField("Check Availability")
    
class ZooBookingForm(FlaskForm):
    forename = StringField('Forename', validators=[DataRequired()])
    surname = StringField('Surname', validators=[DataRequired()])
    email = StringField('E-Mail for Confirmation', validators=[DataRequired(), Email()])
    people = IntegerField('Amount of people', validators=[DataRequired()])
    arrival = DateField('Date of Visit', validators=[DataRequired()])
    submit = SubmitField("Check Availability")
    
class PaymentForm(FlaskForm):
    card_num = StringField('Card Number', validators=[DataRequired(), Length(16, 16)])
    card_expiry_mm = StringField('Card Expiry Month', validators=[DataRequired(), Length(2, 2)])
    card_expiry_yy = StringField('Card Expiry Year', validators=[DataRequired(), Length(2, 2)]) 
    card_cvv = StringField('CVV', validators=[DataRequired(), Length(3, 3)])
    cardholder_name = StringField('Cardholder Name', validators=[DataRequired()])
    save_info = BooleanField('Save your card info to your account?')
    submit = SubmitField("Pay now!")
    
class ProfileForm(FlaskForm):
    home_num = StringField('Street Number', validators=[Length(0, 4)])
    home_street = StringField('Street Name')
    home_town = StringField('Town/City')
    home_postcode = StringField('Postcode', validators=[Length(0, 9)])
    opendyslexic = BooleanField('Dyslexic Readable Text Support?')
    submit = SubmitField("Save Changes")
    
class ContactForm(FlaskForm):
    text = TextAreaField('Your Enquiry', validators=[DataRequired()])
    submit = SubmitField('Send.')