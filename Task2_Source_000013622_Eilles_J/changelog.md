# Changelog
## Jack Eilles - LL-000013622 - Started Task2 15/04/2024

### 15/04/2024

Started initial setup of application, small amount of front-end work was done to make the application functional and give a good starting point.

Log-in and registration system is almost done from today, only addition to add is the registration system, refer to grinberg blog for this, under title User Registration. <- Personal note.

Goals for next session are to have a basic booking system completed, I can't imagine it'll be too much of a struggle, the system will cover both Hotel and Zoo. I may also work on implementing the extra user information like accessibility info and card / address information.

END OF LOG FOR 15/04/2024.

## 17/04/2024

Began work on booking system, primarily front-end work, also finished up user registration system and added statistics logging functionality.

I think I will leave the profile stuff until this booking system is entirely done and stable, after the profile stuff I'll need to start working on the front-end as well as the admin dashboard.

Goals for next session are to get the booking system for both hotel and zoo completed fully, we can add the lookups later on in the admin dash.

END OF LOG FOR 17/04/2024.

## 18/04/2024

Booking system is almost done, I am currently part way through writing the zoo booking logic, in the payment section, I do still need to write the logic behind saving card info if an entity linked to your userid hasn't been found in the "payment_info" table in the db

As we're about a quarter of the way in, I'd say the progress I'm making is quite good, will try and get the booking system fully done as of next session, which should be tomorrow.

note before end bc you got extra time, booking sys is done, BUT FIX THE AWAITING PAYMENT, THAT MF BROKENNNNN ^_^.

END OF LOG FOR 18/04/2024
 NOTE FROM 19/04: im stupid, the awaiting payment thing was just the wrong way around ugh

## 19/04/2024

Home page styling is messed up to all hell, fix it.
Booking system done tho. Functional w no bugs.

## 22/04/2024

Okay so we've not done too much today, added a few cosmetic pages but thats it. We need to add the messaging system in the next session, that is a MASSIVE priority, itll be fairly quick, should only take about 30 mins or so to implement.

As for the loyalty system, that will take like 15 mins, you can do that too.

Admin dashboard as well as info pages need completed and polished up, but thats a project for over the next few days.

## 25/04/2024

LAST SESSION MORNING YOU NEED TO FINISH ABOUT THE ZOO AND ADD REWARDS USING
TNC, PRIV POL, COOKIE POL
do dev docs, test log, video, asset log and dockerfile
THAT IS ITTTTTT

## 26/04/2024

Ok come back and quickly finish checking bookings thingy.