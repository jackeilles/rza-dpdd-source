# Config for RZA App

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'l7qZHJZX4U9Fklq3innkNNM8H4S9-ORpUHWY2Uuv3p8' # Randomly generated from https://generate.plus/en/base64