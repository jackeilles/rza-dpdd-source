"""i broke it so here we go again

Revision ID: 6ee71c9259ba
Revises: 
Create Date: 2024-04-18 10:03:42.143142

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6ee71c9259ba'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('statistics',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('date', sa.String(length=16), nullable=False),
    sa.Column('zoo_bookings', sa.Integer(), nullable=True),
    sa.Column('hotel_bookings', sa.Integer(), nullable=True),
    sa.Column('user_signups', sa.Integer(), nullable=True),
    sa.Column('site_visits', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('statistics', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_statistics_date'), ['date'], unique=False)
        batch_op.create_index(batch_op.f('ix_statistics_hotel_bookings'), ['hotel_bookings'], unique=False)
        batch_op.create_index(batch_op.f('ix_statistics_site_visits'), ['site_visits'], unique=False)
        batch_op.create_index(batch_op.f('ix_statistics_user_signups'), ['user_signups'], unique=False)
        batch_op.create_index(batch_op.f('ix_statistics_zoo_bookings'), ['zoo_bookings'], unique=False)

    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('forename', sa.String(length=32), nullable=False),
    sa.Column('surname', sa.String(length=32), nullable=False),
    sa.Column('email', sa.String(length=64), nullable=False),
    sa.Column('password_hash', sa.String(length=256), nullable=True),
    sa.Column('home_num', sa.Integer(), nullable=True),
    sa.Column('home_street', sa.String(length=64), nullable=True),
    sa.Column('home_town', sa.String(length=128), nullable=True),
    sa.Column('home_postcode', sa.String(length=8), nullable=True),
    sa.Column('opendyslexic', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_user_email'), ['email'], unique=True)
        batch_op.create_index(batch_op.f('ix_user_forename'), ['forename'], unique=False)
        batch_op.create_index(batch_op.f('ix_user_home_num'), ['home_num'], unique=False)
        batch_op.create_index(batch_op.f('ix_user_home_postcode'), ['home_postcode'], unique=False)
        batch_op.create_index(batch_op.f('ix_user_home_street'), ['home_street'], unique=False)
        batch_op.create_index(batch_op.f('ix_user_home_town'), ['home_town'], unique=False)
        batch_op.create_index(batch_op.f('ix_user_opendyslexic'), ['opendyslexic'], unique=False)
        batch_op.create_index(batch_op.f('ix_user_surname'), ['surname'], unique=False)

    op.create_table('hotel_bookings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('booking_info', sa.JSON(), nullable=False),
    sa.Column('has_zoo_booking', sa.Boolean(), nullable=False),
    sa.Column('awaiting_payment', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('hotel_bookings', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_hotel_bookings_awaiting_payment'), ['awaiting_payment'], unique=False)
        batch_op.create_index(batch_op.f('ix_hotel_bookings_has_zoo_booking'), ['has_zoo_booking'], unique=False)
        batch_op.create_index(batch_op.f('ix_hotel_bookings_user_id'), ['user_id'], unique=False)

    op.create_table('payment_info',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('card_info', sa.JSON(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('payment_info', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_payment_info_user_id'), ['user_id'], unique=False)

    op.create_table('zoo_bookings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('booking_info', sa.JSON(), nullable=False),
    sa.Column('has_hotel_booking', sa.Boolean(), nullable=False),
    sa.Column('awaiting_payment', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('zoo_bookings', schema=None) as batch_op:
        batch_op.create_index(batch_op.f('ix_zoo_bookings_awaiting_payment'), ['awaiting_payment'], unique=False)
        batch_op.create_index(batch_op.f('ix_zoo_bookings_has_hotel_booking'), ['has_hotel_booking'], unique=False)
        batch_op.create_index(batch_op.f('ix_zoo_bookings_user_id'), ['user_id'], unique=False)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('zoo_bookings', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_zoo_bookings_user_id'))
        batch_op.drop_index(batch_op.f('ix_zoo_bookings_has_hotel_booking'))
        batch_op.drop_index(batch_op.f('ix_zoo_bookings_awaiting_payment'))

    op.drop_table('zoo_bookings')
    with op.batch_alter_table('payment_info', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_payment_info_user_id'))

    op.drop_table('payment_info')
    with op.batch_alter_table('hotel_bookings', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_hotel_bookings_user_id'))
        batch_op.drop_index(batch_op.f('ix_hotel_bookings_has_zoo_booking'))
        batch_op.drop_index(batch_op.f('ix_hotel_bookings_awaiting_payment'))

    op.drop_table('hotel_bookings')
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_user_surname'))
        batch_op.drop_index(batch_op.f('ix_user_opendyslexic'))
        batch_op.drop_index(batch_op.f('ix_user_home_town'))
        batch_op.drop_index(batch_op.f('ix_user_home_street'))
        batch_op.drop_index(batch_op.f('ix_user_home_postcode'))
        batch_op.drop_index(batch_op.f('ix_user_home_num'))
        batch_op.drop_index(batch_op.f('ix_user_forename'))
        batch_op.drop_index(batch_op.f('ix_user_email'))

    op.drop_table('user')
    with op.batch_alter_table('statistics', schema=None) as batch_op:
        batch_op.drop_index(batch_op.f('ix_statistics_zoo_bookings'))
        batch_op.drop_index(batch_op.f('ix_statistics_user_signups'))
        batch_op.drop_index(batch_op.f('ix_statistics_site_visits'))
        batch_op.drop_index(batch_op.f('ix_statistics_hotel_bookings'))
        batch_op.drop_index(batch_op.f('ix_statistics_date'))

    op.drop_table('statistics')
    # ### end Alembic commands ###
