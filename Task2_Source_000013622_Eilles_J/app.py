#!/usr/bin/env python3

from app import app
from waitress import serve

# Run app using waitress or flask (uncomment to switch)
if __name__ == "__main__":
    serve(app, listen='*:5000') # Waitress
    # app.run(host='0.0.0.0', port='5000', debug=True) # Flask during dev