Hello, to whomever may be looking through these files.
To run this as an app, you can either do it the manual way through Python alone, or you can use Docker to containerise it.


 ____  ____  ____  _  __ _____ ____ 
/  _ \/  _ \/   _\/ |/ //  __//  __\
| | \|| / \||  /  |   / |  \  |  \/|  This method
| |_/|| \_/||  \_ |   \ |  /_ |    /  is recommended
\____/\____/\____/\_|\_\\____\\_/\_\
                                    
To run this using Docker, you will firstly need to navigate to ./Task2_Prototype_000013622_Eilles_J/
Open a Terminal in that directory and run the below commands:

docker load -i Task2_Docker_000013622_Eilles_J
docker run -p 5000:5000 rza

Then if you navigate to http://localhost:5000 you should see the website.



 ____ ___  _ _____  _     ____  _     
/  __\\  \///__ __\/ \ /|/  _ \/ \  /|
|  \/| \  /   / \  | |_||| / \|| |\ ||  If you don't have
|  __/ / /    | |  | | ||| \_/|| | \||  Docker installed
\_/   /_/     \_/  \_/ \|\____/\_/  \|
                                      
To run this using Python alone, navigate to ./Task2_Source_000013622_Eilles_J/
Open a Terminal in this directory and run the following commands:

python -m venv .venv

# If you are on Linux/UNIX based OS like MacOS
source .venv/bin/activate
# Or if you are on Windows
.venv/Scripts/activate

pip install -r requirements.txt
python app.py

Then navigate to http://localhost:5000 and you should see the website then.